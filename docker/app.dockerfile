FROM php:7.1.15-fpm

## Add MariaDb repository (UFSCar BR mirror)
RUN apt-get -yqq update && apt-get -yqq install python-software-properties software-properties-common
RUN apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
RUN add-apt-repository 'deb http://mariadb.biz.net.id//repo/10.1/debian jessie main'

## install required dependencies (packages and extensions) so that Laravel 5.6 can run
RUN apt-get -yqq update && apt-get -yqq install mariadb-client --no-install-recommends
RUN docker-php-ext-install -j4 pdo_mysql

## set right permissions for application folder
RUN chown -R www-data:www-data /var/www
